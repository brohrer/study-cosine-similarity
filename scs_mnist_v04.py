import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers

from absolute_pooling import MaxAbsPool2D
from one_cycle import OneCycleScheduler
from sharpened_cosine_similarity_v01 import CosSim2D


version = "04"
batch_size = 1024
epochs = 5
n_fits = 100
lr = .05

checkpoint_path = f"training_{version}/cp.ckpt"
results_path = f"plots/results_{version}.png"
results_detail_path = f"plots/results_detail_{version}.png"
accuracy_results_path = f"results/accuracy_{version}.txt"
loss_results_path = f"results/loss_{version}.txt"
summary_path = f"results/summary_{version}.txt"
os.makedirs("plots", exist_ok=True)
os.makedirs("results", exist_ok=True)

num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Normalization not necessary with Sharpened Cosine Similarity
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

n_quick = 2 * batch_size
x_train = x_train[:n_quick, : :]
y_train = y_train[:n_quick]
# x_test = x_test[:n_quick, : :]
# y_test = y_test[:n_quick]

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


fig = plt.figure("loss_curves")
ax = fig.gca()
fig_detail = plt.figure("loss_curves_detail")
ax_detail = fig_detail.gca()

ax.set_ylabel("log10(error)")
ax.set_xlabel("Epoch")
ax.set_ylim(-2.1, 0)
ax.grid()

ax_detail.set_ylabel("log10(error)")
ax_detail.set_xlabel("Epoch")
ax_detail.set_ylim(-1.5, -1)
ax_detail.grid()

# Iterate and compare results
loss_results = []
accuracy_results = []
for _ in range(n_fits):
    tf.keras.backend.clear_session()

    model = keras.Sequential(
        [
            layers.InputLayer(input_shape=input_shape),
            layers.Dropout(0.07),
            CosSim2D(3, 10, 1, padding='valid'),
            CosSim2D(1, 10, 1),
            CosSim2D(1, 12, 1),
            layers.Dropout(0.07),
            layers.MaxPool2D((2,2)),
            CosSim2D(3, 2, 1, depthwise_separable=True, padding='same'),
            CosSim2D(1, 8, 1),
            layers.Dropout(0.07),
            layers.MaxPool2D((2,2)),
            CosSim2D(3, 4, 1, depthwise_separable=True),
            CosSim2D(1, 10, 1),
            layers.Dropout(0.07),
            MaxAbsPool2D(2, True),
            layers.Flatten(),
            layers.Dense(num_classes, activation=None),
        ]
    )


    with open(summary_path, "w") as f:
        # Pass the file handle in as a lambda function to make it callable
        model.summary(print_fn=lambda x: f.write(x + '\n'))

    checkpoint_dir = os.path.dirname(checkpoint_path)
    checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_dir,
        save_weights_only=True,
        monitor='val_accuracy',
        save_best_only=True)

    steps = np.ceil(len(x_train) / batch_size) * epochs
    lr_schedule = OneCycleScheduler(lr, steps)

    def get_lr_metric(optimizer):
        def lr(y_true, y_pred):
            return optimizer._decayed_lr(tf.float32)
        return lr


    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

    model.compile(
        loss=loss,
        optimizer=optimizer,
        metrics=[get_lr_metric(optimizer), "accuracy"],
        run_eagerly=False)

    error_history = model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        callbacks=[checkpoint, lr_schedule],
        validation_data=(x_test, y_test))

    model.load_weights(checkpoint_dir)
    evaluation_results = model.evaluate(x_test, y_test)
    print(evaluation_results)
    print(model.metrics_names)
    accuracy_results.append(evaluation_results[2])
    loss_results.append(evaluation_results[0])

    with open(accuracy_results_path, "w") as f:
        for accuracy in accuracy_results:
            f.write(f"{accuracy:.06}\n")
    with open(loss_results_path, "w") as f:
        for loss in loss_results:
            f.write(f"{loss:.06}\n")

    error = 1 - np.array(error_history.history['val_accuracy'])
    log_error = np.log10(error)

    ax.plot(log_error, linewidth=.5, alpha=.8)
    ax_detail.plot(log_error)

    plt.figure("loss_curves")
    plt.savefig(results_path, dpi=300)
    plt.figure("loss_curves_detail")
    plt.savefig(results_detail_path, dpi=300)
