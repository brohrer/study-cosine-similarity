import os
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers

from absolute_pooling import MaxAbsPool2D
from one_cycle import OneCycleScheduler
from sharpened_cosine_similarity_v02 import CosSim2D

version = "00"
batch_size = 1024
epochs = 100
n_training = 10 * batch_size
n_fits = 1000
lr = .05

checkpoint_path = f"training_{version}/cp.ckpt"
accuracy_results_path = f"results/accuracy_{version}.npy"
accuracy_history_path = f"results/accuracy_history_{version}.npy"
loss_results_path = f"results/loss_{version}.npy"
summary_path = f"results/summary_{version}.txt"
os.makedirs("results", exist_ok=True)

num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.fashion_mnist.load_data()

# Normalization not necessary with Sharpened Cosine Similarity
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

x_train = x_train[:n_training, : :]
y_train = y_train[:n_training]
# x_test = x_test[:n_training, : :]
# y_test = y_test[:n_training]

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# Iterate and compare results
loss_results = []
accuracy_results = []
accuracy_histories = []
for _ in range(n_fits):
    tf.keras.backend.clear_session()

    model = keras.Sequential(
        [
            layers.InputLayer(input_shape=input_shape),
            layers.Dropout(0.07),
            CosSim2D(3, 10, 1, padding='valid'),
            CosSim2D(1, 10, 1),
            CosSim2D(1, 12, 1),
            layers.Dropout(0.07),
            layers.MaxPool2D((2,2)),
            CosSim2D(3, 2, 1, depthwise_separable=True, padding='same'),
            CosSim2D(1, 8, 1),
            layers.Dropout(0.07),
            layers.MaxPool2D((2,2)),
            CosSim2D(3, 4, 1, depthwise_separable=True),
            CosSim2D(1, 10, 1),
            layers.Dropout(0.07),
            MaxAbsPool2D(2, True),
            layers.Flatten(),
            layers.Dense(num_classes, activation=None),
        ]
    )


    with open(summary_path, "w") as f:
        # Pass the file handle in as a lambda function to make it callable
        model.summary(print_fn=lambda x: f.write(x + '\n'))

    checkpoint_dir = os.path.dirname(checkpoint_path)
    checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_dir,
        save_weights_only=True,
        monitor='val_accuracy',
        save_best_only=True)

    steps = np.ceil(len(x_train) / batch_size) * epochs
    lr_schedule = OneCycleScheduler(lr, steps)

    def get_lr_metric(optimizer):
        def lr(y_true, y_pred):
            return optimizer._decayed_lr(tf.float32)
        return lr


    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

    model.compile(
        loss=loss,
        optimizer=optimizer,
        metrics=[get_lr_metric(optimizer), "accuracy"],
        run_eagerly=False)

    results_history = model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        callbacks=[checkpoint, lr_schedule],
        validation_data=(x_test, y_test))
    accuracy_history = results_history.history['val_accuracy']
    accuracy_histories.append(accuracy_history)

    model.load_weights(checkpoint_dir)
    evaluation_results = model.evaluate(x_test, y_test)
    accuracy_results.append(evaluation_results[2])
    loss_results.append(evaluation_results[0])

    np.save(accuracy_results_path, np.array(accuracy_results))
    np.save(loss_results_path, np.array(loss_results))
    np.save(accuracy_history_path, np.array(accuracy_histories))
