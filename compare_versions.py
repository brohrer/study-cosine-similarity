import numpy as np
from scipy import stats

version_a = "24"
version_b = "25"

accuracy_a_results_path = f"results/accuracy_{version_a}.npy"
accuracy_b_results_path = f"results/accuracy_{version_b}.npy"

errors_a = 1 - np.load(accuracy_a_results_path)
errors_b = 1 - np.load(accuracy_b_results_path)

print(f"version a mean        : {np.mean(errors_a):.04}")
print(f"version b mean        : {np.mean(errors_b):.04}")
print(f"version a median      : {np.median(errors_a):.04}")
print(f"version b median      : {np.median(errors_b):.04}")
print(f"version a measurements: {errors_a.size}")
print(f"version b measurements: {errors_b.size}")

t_stat, p_value = stats.ttest_ind(
    errors_a,
    errors_b,
    equal_var = False)
print("t-test p-value", p_value)

u_stat, p_value = stats.mannwhitneyu(
    errors_a,
    errors_b,
    use_continuity=True,
    alternative='two-sided')
print("u-test p-value", p_value)
