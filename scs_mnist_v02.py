import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers

from absolute_pooling import MaxAbsPool2D
from one_cycle import OneCycleScheduler
from sharpened_cosine_similarity_v01 import CosSim2D


checkpoint_path = 'training_02/cp.ckpt'
batch_size = 1024
epochs = 100
lr = .02

num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Normalization not necessary with Sharpened Cosine Similarity
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

# n_quick = 4 * batch_size
# x_train = x_train[:n_quick, : :]
# y_train = y_train[:n_quick]
# x_test = x_test[:n_quick, : :]
# y_test = y_test[:n_quick]

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


tf.keras.backend.clear_session()

model = keras.Sequential(
    [
        layers.InputLayer(input_shape=input_shape),
        layers.Dropout(0.07),
        CosSim2D(3, 10, 1, padding='valid'),
        CosSim2D(1, 10, 1),
        CosSim2D(1, 12, 1),
        layers.Dropout(0.07),
        layers.MaxPool2D((2,2)),
        CosSim2D(3, 2, 1, depthwise_separable=True, padding='same'),
        CosSim2D(1, 8, 1),
        layers.Dropout(0.07),
        layers.MaxPool2D((2,2)),
        CosSim2D(3, 4, 1, depthwise_separable=True),
        CosSim2D(1, 10, 1),
        layers.Dropout(0.07),
        MaxAbsPool2D(2, True),
        layers.Flatten(),
        layers.Dense(num_classes, activation=None),
    ]
)

model.summary()

checkpoint_dir = os.path.dirname(checkpoint_path)
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_dir,
    save_weights_only=True,
    monitor='val_accuracy',
    save_best_only=True)

steps = np.ceil(len(x_train) / batch_size) * epochs
lr_schedule = OneCycleScheduler(lr, steps)

def get_lr_metric(optimizer):
    def lr(y_true, y_pred):
        return optimizer._decayed_lr(tf.float32)
    return lr


optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

model.compile(
    loss=loss,
    optimizer=optimizer,
    metrics=[get_lr_metric(optimizer), "accuracy"],
    run_eagerly=False)

# TODO: iterate and compare results
error_history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=[checkpoint, lr_schedule],
    validation_data=(x_test, y_test))

model.load_weights(checkpoint_dir)
model.evaluate(x_test, y_test)

# print(np.log10(1 - np.array(error_history.history['val_accuracy'])))
error = 1 - np.array(error_history.history['val_accuracy'])
log_error = np.log10(error)
fig = plt.figure()
ax = fig.gca()
ax.plot(log_error)
ax.set_ylabel("log10(error)")
ax.set_xlabel("Epoch")
ax.set_ylim(-2.1, -1.6)
ax.grid()
# TODO: savefig
plt.show()
