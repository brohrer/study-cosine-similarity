import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers

from absolute_pooling import MaxAbsPool2D
from sharpened_cosine_similarity_v00 import CosSim2D


num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Normalization not necessary with Sharpened Cosine Similarity
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


tf.keras.backend.clear_session()

model = keras.Sequential(
    [
        layers.InputLayer(input_shape=input_shape),
        layers.Dropout(0.07),
        CosSim2D(3, 10, 1, padding='valid'),
        CosSim2D(1, 10, 1),
        CosSim2D(1, 12, 1),
        layers.Dropout(0.07),
        layers.MaxPool2D((2,2)),
        CosSim2D(3, 2, 1, depthwise_separable=True, padding='same'),
        CosSim2D(1, 8, 1),
        layers.Dropout(0.07),
        layers.MaxPool2D((2,2)),
        CosSim2D(3, 4, 1, depthwise_separable=True),
        CosSim2D(1, 10, 1),
        layers.Dropout(0.07),
        MaxAbsPool2D(2, True),
        layers.Flatten(),
        layers.Dense(num_classes, activation=None),
    ]
)

model.summary()

batch_size = 1024
epochs = 200
checkpoint_path = 'training_00/cp.ckpt'
checkpoint_dir = os.path.dirname(checkpoint_path)
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_dir,
    save_weights_only=True,
    monitor='val_accuracy',
    save_best_only=True)


lr = tf.keras.optimizers.schedules.CosineDecay(
    0.05,
    decay_steps=(x_train.shape[0] // batch_size) * epochs,
    alpha=0.00001)

def get_lr_metric(optimizer):
    def lr(y_true, y_pred):
        return optimizer._decayed_lr(tf.float32)
    return lr


optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

model.compile(loss=loss, optimizer=optimizer, metrics=[get_lr_metric(optimizer), "accuracy"], run_eagerly=False)

error_history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=[checkpoint],
    validation_data=(x_test, y_test))

model.load_weights(checkpoint_dir)
model.evaluate(x_test, y_test)

for i, layer in enumerate(model.layers):
    if layer.name.startswith('cos_sim2d'):
        kernel_layer = i
        break

_, axs = plt.subplots(2, 5, figsize=(20, 10))
axs = axs.flatten()

kernels = []

for i, ax in zip(range(tf.shape(model.layers[kernel_layer].w)[-1]), axs):
    kernel = model.layers[kernel_layer].w[0,:, i]
    side = tf.sqrt(tf.cast(tf.shape(kernel)[0], tf.float32))
    kernel = tf.reshape(kernel, (side,side,1)).numpy()
    kernels.append(kernel)
    ax.imshow(kernel[:,:,0], vmin=-1, vmax=1)
    ax.axis('off')
plt.tight_layout()
plt.show()

n = 9
print(layer)
plt.imshow(x_test[n][:,:,0])
plt.axis('off')
plt.show()

activations = layer(x_test[n:n+1])[0].numpy()

_, axs = plt.subplots(nrows=activations.shape[-1], ncols=2, figsize=(15, activations.shape[-1] * 8))

for i in range(10):
    axs[i, 0].imshow(activations[:,:,i], vmin=-1, vmax=1)
    axs[i, 0].axis('off')
    axs[i, 1].imshow(kernels[i][:,:,0], vmin=-1, vmax=1)
    axs[i, 1].axis('off')
plt.tight_layout
plt.show()
