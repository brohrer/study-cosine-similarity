import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

version = "05"
accuracy_results_path = f"results/accuracy_{version}.npy"
p_p_plot_path = f"plots/p_p_plot_{version}.png"
fig = plt.figure("p_p_plot")
ax = fig.gca()
accuracy_results = np.load(accuracy_results_path)
errors = 1 - accuracy_results
errors = np.sort(errors)
cumulative_probability = (np.cumsum(np.ones(errors.size)) - .5) / errors.size

z_errors = (errors - np.mean(errors)) / np.var(errors) ** .5
p_errors = stats.norm.cdf(z_errors)
plt.plot(
    p_errors,
    cumulative_probability,
    marker="o",
    markersize=4,
    linewidth=0)
plt.plot([0, 1], [0, 1], color="black", linewidth=.5, zorder=-1)
plt.show()
# plt.savefig(p_p_plot_path, dpi=300)
